import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from "react-native";
import {connect} from "react-redux";

class Calculator extends Component {
    render() {
        return (
            <View style={styles.calculator}>
                <View style={styles.itemName}>
                    <Text style={styles.itemText}>{this.props.ctr ? this.props.ctr : 0}</Text>
                </View>
                <View style={styles.base}>
                    <TouchableOpacity  style={styles.add} onPress={() => this.props.addNumber('1')}>
                        <Text style={styles.add}>1</Text>
                    </TouchableOpacity>
                    <TouchableOpacity  style={styles.add} onPress={() => this.props.addNumber('2')}>
                        <Text style={styles.add}>2</Text>
                    </TouchableOpacity>
                    <TouchableOpacity  style={styles.add} onPress={() => this.props.addNumber('3')}>
                        <Text style={styles.add}>3</Text>
                    </TouchableOpacity>
                    <TouchableOpacity  style={styles.add} title='-' onPress={() => this.props.addNumber('-')}>
                        <Text style={styles.add}>-</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.base}>
                    <TouchableOpacity style={styles.add} title='4' onPress={() => this.props.addNumber('4')}>
                        <Text style={styles.add}>4</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.add} title='5' onPress={() => this.props.addNumber('5')}>
                        <Text style={styles.add}>5</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.add} title='6' onPress={() => this.props.addNumber('6')}>
                        <Text style={styles.add}>6</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.add} title='+' onPress={() => this.props.addNumber('+')}>
                        <Text style={styles.add}>+</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.base}>
                    <TouchableOpacity style={styles.add} title='7' onPress={() => this.props.addNumber('7')}>
                        <Text style={styles.add}>7</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.add} title='8' onPress={() => this.props.addNumber('8')}>
                        <Text style={styles.add}>8</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.add} title='9' onPress={() => this.props.addNumber('9')}>
                        <Text style={styles.add}>9</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.add} title='*' onPress={() => this.props.addNumber('*')}>
                        <Text style={styles.add}>*</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.base}>
                    <TouchableOpacity style={styles.add} title='AC' onPress={this.props.clear}>
                        <Text style={styles.add}>AC</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.add} title='0' onPress={() => this.props.addNumber('0')}>
                        <Text style={styles.add}>0</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.add} title='/' onPress={() => this.props.addNumber('/')}>
                        <Text style={styles.add}>/</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.add}title='=' onPress={this.props.equal}>
                        <Text style={styles.add}>=</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    calculator: {
        justifyContent: 'flex-start',
        maxWidth: 500,
        flex: 1,
        backgroundColor: '#ccc',
        overflow: 'scroll',
    },

    itemName: {
        alignItems: 'stretch',
        color: '#000',
        padding: 20,
        borderWidth: 1,
        borderColor: '#000',
        display: 'flex',
        flexDirection: 'column-reverse',
    },

    itemText: {
        textAlign: 'right',
        fontSize: 30,
        flex: ''
    },

    add: {
        textAlign: 'center',
        padding: 10,
        backgroundColor: '#000',
        borderWidth: 1,
        borderColor: '#ccc',
        fontSize: 30,
        color: '#fff',
        fontWeight: 'bold',
        flexGrow: 1,
    },

    base: {
        flexDirection: 'row',
        // display: 'flex',
        // alignSelf: 'auto',
    }
});

const mapStateToProps = state => {
    return {
        ctr: state.number,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        equal: () => dispatch({type: 'EQUAL'}),
        clear: () => dispatch({type: 'CLEAR'}),
        addNumber: value => dispatch({type: 'ADD', value}),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Calculator);