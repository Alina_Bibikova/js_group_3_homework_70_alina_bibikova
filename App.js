import React from 'react';
import {createStore} from 'redux';
import reducer from "./store/reducer";
import {Provider} from "react-redux";
import Calculator from "./containers/Calculator/Calculator";



const store = createStore(reducer);

export default class App extends React.Component {
  render() {
    return (
        <Provider store={store}>
            <Calculator />
        </Provider>
    )
  }
}


