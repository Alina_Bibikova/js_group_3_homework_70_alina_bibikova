const initialState = {
    number: '',
};

const reducer = (state = initialState, action) => {
    if (action.type === 'CLEAR') {
            return {
                ...state,
                number: '',
            };
        }

    if (action.type === 'EQUAL') {
        let result = state.number;

        try {
            result = eval(state.number);
        } catch (e) {
            console.log(e);
        }

        return {
            ...state,
            number: state.number = result,
        };
    }

    if (action.type === 'ADD') {
        return {
            ...state,
            number: state.number + action.value
        }
    }

    return state;
};

export default reducer;